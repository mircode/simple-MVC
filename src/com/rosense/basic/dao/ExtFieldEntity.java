package com.rosense.basic.dao;

import java.util.Date;

import javax.persistence.MappedSuperclass;

/**
 * 
 * @author 李岩飞
 * @email eliyanfei@126.com	
 * 2015年8月18日 下午8:13:52
 *
 */
@MappedSuperclass
public class ExtFieldEntity extends IdEntity {
	private String userId; //创建者名称
	private Date created; //创建日期

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getUserId() {
		return userId;
	}

	public Date getCreated() {
		return created;
	}

	public void setCreated(Date created) {
		this.created = created;
	}

}
