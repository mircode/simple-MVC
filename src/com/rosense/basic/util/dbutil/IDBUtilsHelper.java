package com.rosense.basic.util.dbutil;

import org.apache.commons.dbutils.QueryRunner;

/**
 * 
 * @author 李岩飞
 * @email eliyanfei@126.com	
 * 2015年8月18日 下午8:16:15
 *
 */
public interface IDBUtilsHelper {
	public QueryRunner getQr();
}
