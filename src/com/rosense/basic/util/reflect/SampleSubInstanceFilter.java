package com.rosense.basic.util.reflect;

import java.lang.reflect.Modifier;

/**
 * 
 * @author 李岩飞
 * @email eliyanfei@126.com	
 * 2015年8月31日 下午1:04:43
 *
 */
public class SampleSubInstanceFilter extends SampleSubTypeFilter {
	public static final ISubTypeFilter DEFAULT = new SampleSubInstanceFilter();

	public SampleSubInstanceFilter() {
	}

	@Override
	protected boolean filterForInstance(final Class<?> clazz) {
		final int modifier = clazz.getModifiers();
		return (!Modifier.isAbstract(modifier) && !Modifier.isInterface(modifier));
	}
}
