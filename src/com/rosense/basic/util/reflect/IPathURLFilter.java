package com.rosense.basic.util.reflect;

import java.net.URL;

/**
 * 
 * @author 李岩飞
 * @email eliyanfei@126.com	
 * 2015年8月31日 下午1:05:24
 *
 */
public interface IPathURLFilter {
	public boolean accept(URL pathURL);
}
