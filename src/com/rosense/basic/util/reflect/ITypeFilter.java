package com.rosense.basic.util.reflect;

/**
 * 
 * @author 李岩飞
 * @email eliyanfei@126.com	
 * 2015年8月31日 下午1:05:16
 *
 */
public interface ITypeFilter {
	public boolean accept(String typeName);
}
