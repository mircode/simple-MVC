package com.rosense.module.system.web.form;

import java.io.Serializable;

import com.rosense.basic.model.EasyuiTree;

public class PositionForm extends EasyuiTree<PositionForm> implements Serializable {
	private static final long serialVersionUID = 1L;
	private String name; //岗位名称
	private int sort;

	public void setSort(int sort) {
		this.sort = sort;
	}

	public int getSort() {
		return sort;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
