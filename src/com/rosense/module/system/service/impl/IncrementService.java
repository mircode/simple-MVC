package com.rosense.module.system.service.impl;

import javax.inject.Inject;
import javax.transaction.Transactional;

import org.springframework.stereotype.Service;

import com.rosense.basic.dao.IBaseDao;
import com.rosense.module.system.entity.IncrementEntity;
import com.rosense.module.system.service.IIncrementService;

/**
 * 
 * @author 李岩飞
 * @email eliyanfei@126.com	
 * 2016年5月12日 上午11:18:00
 *
 */
@Service("increment")
@Transactional
public class IncrementService implements IIncrementService {
	@Inject
	private IBaseDao<IncrementEntity> incrementDao;

	@Override
	public int nextId(String id) {
		IncrementEntity entity = (IncrementEntity) this.incrementDao.queryObject("from IncrementEntity where id=?", id);
		if (entity == null) {
			entity = new IncrementEntity();
			entity.setValue(1);
			entity.setId(id);
			this.incrementDao.add(entity);
		} else {
			entity.setValue(entity.getValue() + 1);
			this.incrementDao.update(entity);
		}
		return entity.getValue();
	}

}
