package com.rosense.module.system.service.impl;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.rosense.basic.dao.IBaseDao;
import com.rosense.basic.exception.ServiceException;
import com.rosense.basic.model.Msg;
import com.rosense.basic.util.BeanUtils;
import com.rosense.basic.util.StringUtil;
import com.rosense.module.common.service.BaseService;
import com.rosense.module.common.web.servlet.WebContextUtil;
import com.rosense.module.system.entity.OrgEntity;
import com.rosense.module.system.service.IOrgService;
import com.rosense.module.system.web.form.OrgForm;

/**
 * @author 李岩飞
 * @email eliyanfei@126.com	
 * 2016年7月23日 下午9:12:15
 * 
 */
@Service("orgService")
@Transactional
public class OrgService extends BaseService implements IOrgService {
	@Inject
	private IBaseDao<OrgEntity> basedaoOrg;

	@Override
	public Msg add(OrgForm form) {
		if (this.getByName(form.getName()) != null)
			throw new ServiceException("该机构或部门的名称[" + form.getName() + "]已存在，无法添加！");
		final OrgEntity org = new OrgEntity();
		BeanUtils.copyNotNullProperties(form, org);
		org.setSort(WebContextUtil.getNextId("org"));
		if (StringUtil.isNotEmpty(form.getPid())) {
			org.setPid(form.getPid());
		}else{
			org.setPid(null);
		}

		this.basedaoOrg.add(org);
		this.logService.add("添加部门", "名称：[" + org.getName() + "]");
		form.setId(org.getId());
		return new Msg(true, "添加成功！");
	}

	@Override
	public OrgForm getByName(String name) {
		List<OrgEntity> list = this.basedaoOrg.list("from OrgEntity where name=?", name);
		if (list != null && list.size() > 0) {
			OrgForm orgForm = new OrgForm();
			BeanUtils.copyNotNullProperties(list.get(0), orgForm);
			return orgForm;
		}
		return null;
	}

	@Override
	public Msg delete(OrgForm form) {
		OrgEntity entity = this.basedaoOrg.load(OrgEntity.class, form.getId());
		del(entity);
		return new Msg(true, "删除成功！");
	}

	private void del(OrgEntity org) {
		List<OrgEntity> list = this.basedaoOrg.list("from OrgEntity where pid=?", org.getPid());
		for (OrgEntity e : list) {
			del(e);
		}
		this.basedaoOrg.delete(OrgEntity.class, org.getId());
		this.basedaoOrg.executeSQL("update simple_person set orgId=? where orgId=?", new Object[] { null, org.getId() });
		this.logService.add("删除部门", "名称：[" + org.getName() + "]");
	}

	@Override
	public Msg update(OrgForm form) {
		OrgEntity org = this.basedaoOrg.load(OrgEntity.class, form.getId());
		BeanUtils.copyNotNullProperties(form, org);
		if (null != form.getPid() && !"".equals(form.getPid())) {
			if (!org.getId().equals(form.getPid())) {
				org.setPid(form.getPid());
			} else {
				return new Msg(false, "操作有误，父模块服务关联自己！");
			}
		}
		this.basedaoOrg.update(org);
		this.logService.add("修改部门", "名称：[" + org.getName() + "]");
		return new Msg(true, "修改成功！");
	}

	@Override
	public OrgForm get(OrgForm form) {
		OrgEntity entity = this.basedaoOrg.load(OrgEntity.class, form.getId());
		BeanUtils.copyNotNullProperties(entity, form);
		form.setPid(entity.getPid());
		return form;
	}

	@Override
	public List<OrgForm> tree(String pid) {
		String sql = "select t.* from simple_org t where t.pid is null order by t.sort asc";
		if (null != pid && !"".equals(pid.trim()))
			sql = "select t.* from simple_org t where t.id='" + pid + "'";

		List<OrgForm> list = this.basedaoOrg.listSQL(sql, OrgForm.class, false);
		List<OrgForm> forms = new ArrayList<OrgForm>();
		for (OrgForm e : list) {
			forms.add(recursive(e));
		}
		return forms;
	}

	public OrgForm recursive(OrgForm form) {
		form.setText(form.getName());
		List<OrgForm> orgs = this.basedaoOrg.listSQL("select t.* from simple_org t where t.pid='" + form.getId() + "' order by t.sort asc", OrgForm.class, false);
		if (null != orgs && orgs.size() > 0) {
			List<OrgForm> chlds = new ArrayList<OrgForm>();
			for (OrgForm e : orgs) {
				OrgForm recursive = this.recursive(e);

				OrgForm childform = new OrgForm();
				BeanUtils.copyNotNullProperties(recursive, childform);
				chlds.add(childform);
			}
			form.setChildren(chlds);
		}
		return form;
	}

}
