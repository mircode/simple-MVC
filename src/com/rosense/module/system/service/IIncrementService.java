package com.rosense.module.system.service;

/**
 * 微信相关操作
 * @author 李岩飞
 * @email eliyanfei@126.com	
 * 2015年8月24日 下午3:32:03
 *
 */
public interface IIncrementService {
	int nextId(String id);
}
