package com.rosense.module.system.entity;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import com.rosense.basic.dao.IdEntity;

/**
 * 用户实体
 */
@Entity
@Table(name = "simple_user")
public class UserEntity extends IdEntity {
	private String name; //姓名
	private String account; //登陆账号
	private String password = ""; //登陆密码
	private Integer status = new Integer(0); //账号状态（0：允许登录，1：禁止登录）
	private String personId;//详细用户Id
	private Set<RoleEntity> roles = new HashSet<RoleEntity>(0); //权限角色
	private long lastLoginTime = 0;//最后登录时间
	private Date created;//创建时间

	public UserEntity() {
		this.created = new Date();
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setCreated(Date created) {
		this.created = created;
	}

	public Date getCreated() {
		return created;
	}

	public void setLastLoginTime(long lastLoginTime) {
		this.lastLoginTime = lastLoginTime;
	}

	public long getLastLoginTime() {
		return lastLoginTime;
	}

	@ManyToMany(fetch = FetchType.LAZY)
	@JoinTable(name = "simple_user_roles", joinColumns = { @JoinColumn(name = "userId", nullable = false, updatable = false) }, inverseJoinColumns = { @JoinColumn(name = "roleId", nullable = false, updatable = false) })
	public Set<RoleEntity> getRoles() {
		return roles;
	}

	public void setRoles(Set<RoleEntity> roles) {
		this.roles = roles;
	}

	public void setPersonId(String personId) {
		this.personId = personId;
	}

	public String getPersonId() {
		return personId;
	}

	public String getAccount() {
		return account;
	}

	public void setAccount(String account) {
		this.account = account;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

}
