<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width,initial-scale=1">
<title>Simple MVC</title>
<meta name="description" content="Simple MVC是基于Spring MVC，Hibernate，Boostrap可以快速开发的系统框架">
<meta name="keyworkds" content="Lodash,Lodashjs,underscore,underscorejs,javascript,lib,module">
<link rel="stylesheet" href="/template/resource/css/bootstrap.min.css" />
<link rel="stylesheet" href="/template/doc.css" />
<link rel="stylesheet" href="/template/resource/plugins/font-awesome/css/font-awesome.min.css" />
<script src="/template/resource/js/jquery-2.1.4.min.js"></script>
	<script src="/template/resource/js/bootstrap.min.js"></script>
	<script src="/template/updatelog.js"></script>
	<script src="/template/resource/js/simple.js"></script>
<style>
<!--
body {
	background-image: url("/template/images/bg.gif");
}

.container {
	width: 1000px;
	margin: 0 auto;
	background-color: white;
	-webkit-box-shadow: 0 0 2.5em #5d656c;
	-moz-box-shadow: 0 0 2.5em #5d656c;
	box-shadow: 0 0 2.5em #5d656c;
}
-->
</style>
</head>
<body>
	<div class="container">
		<nav class="navbar navbar-default">
			<div class="container-fluid">
				<!-- Brand and toggle get grouped for better mobile display -->
				<div class="navbar-header">
					<a class="navbar-brand" href="#">Simple MVC</a>
				</div>

				<!-- Collect the nav links, forms, and other content for toggling -->
				<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
					<ul class="nav navbar-nav navbar-right">
						<li><a href="/login.jsp">登录</a></li>
					</ul>
				</div>
				<!-- /.navbar-collapse -->
			</div>
			<!-- /.container-fluid -->
		</nav>
		<h1 style="margin-bottom: 10px;">
			Simple MVC <span>v1.0.0</span>
		</h1>
		<h3 class="description">
			Simple MVC是基于Spring MVC，Hibernate，Boostrap3等可以快速开发的系统框架,该结构以及集成了基本的组织架构，以及详细的权限控制功能。如果你也是想基于Spring MVC做开发，可以<a href="https://git.oschina.net/eliyanfei/simple-MVC">下载Simple MVC</a>做二次开发。
		</h3>
		<p>主要配置</p>
		<pre>
src/
├── config/
│   ├── db.properties
│   ├── bootstrap.min.css
│   ├── spring-datasource.xml
│   ├── spring-hibernate.xml
│   └── spring-mvc.xml
├── ftl/
│   └── *.ftl
└── init/
    ├── caches.xml
    ├── init.xml
    └── initmenu.xml
	</pre>
		<div class="bs-docs-section">
			<div class="bs-example" data-example-id="badges-in-pills">db.properties数据库基本配置</div>
			<div class="highlight">
				<pre>
				<code class="language-html" data-lang="html">
###########DataBase MySQL###########
driverClassName=com.mysql.jdbc.Driver
url=jdbc\:mysql\://localhost\:3306/my?characterEncoding\=UTF-8
username=root
password=root
......
				</code>
			</pre>
			</div>
		</div>
		<div class="bs-docs-section">
			<div class="bs-example" data-example-id="badges-in-pills">spring-datasource.xml,spring-hibernate.xml,spring-mvc.xml这三个文件为Hibernate以及Spring的配置</div>
			<div class="highlight">
				<pre>
			</pre>
			</div>
		</div>
		<div class="bs-docs-section">
			<div class="bs-example" data-example-id="badges-in-pills">ftl/*.ftl为Freemarker的模板配置，主要用于发送邮件</div>
			<div class="highlight">
				<pre>
			</pre>
			</div>
		</div>
		<div class="bs-docs-section">
			<div class="bs-example" data-example-id="badges-in-pills">caches.xml为缓存配置依赖ehcache.jar文件</div>
			<div class="highlight">
				<pre>
				<code class="language-html" data-lang="html">
cacheType="map_string"（key-map）
SELECT U.ID AS CACHEID,U.ID,U.NAME,U.PERSONID,P.PHOTO FROM SIMPLE_USER U LEFT JOIN SIMPLE_PERSON P ON U.PERSONID=P.ID
Map<String , String> map = Caches.getMap("USER", "01");
cacheType="string"（key-value）
SELECT C.id AS CACHEID,c.name CACHEVALUE FROM  SIMPLE_USER C   
Object value = Caches.get("USER", "01");
					</code>
			</pre>
			</div>
		</div>
		<div class="bs-docs-section">
			<div class="bs-example" data-example-id="badges-in-pills">init.xml初始化基本属性，比如角色。</div>
			<div class="highlight">
				<pre>
			</pre>
			</div>
		</div>
		<div class="bs-docs-section">
			<div class="bs-example" data-example-id="badges-in-pills">initmenu.xml初始化菜单，功能菜单。</div>
			<div class="highlight">
				<pre>
			</pre>
			</div>
		</div>
		<script src='http://git.oschina.net/eliyanfei/simple-MVC/widget_preview'></script>

<style>
.pro_name a{color: #4183c4;}
.osc_git_title{background-color: #d8e5f1;}
.osc_git_box{background-color: #fafafa;}
.osc_git_box{border-color: #ddd;}
.osc_git_info{color: #666;}
.osc_git_main a{color: #4183c4;}
</style>
		<jsp:include page="/WEB-INF/pages/app/bbs.jsp"></jsp:include>
		<footer class="bs-docs-footer"> Copyright © 2014 京ICP备14055094号(1-1) </footer>
	</div>
</body>
</html>