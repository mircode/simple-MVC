<%@page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<jsp:include page="/template/modal.jsp"><jsp:param value="addroles" name="id" /><jsp:param value="编辑角色" name="title" /></jsp:include>
<section class="content">
	<div id="user-form-id"></div>
	<div id="filter-bar"></div>
	<table id="user_table" class="table-condensed table table-hover" data-row-style="rowStyle" data-side-pagination="server"></table>
</section>
<script type="text/javascript">
	var $user_table;
	$(function() {
		$user_table = $.BOOT.table("user_table", $.webapp.root
				+ "/admin/system/user/datagrid.do", {
			columns : [ {
				field : 'account',
				title : '账号'
			}, {
				field : 'name',
				title : '姓名'
			}, {
				field : 'role_names',
				title : '角色'
			}, {
				field : 'status',
				title : '状态',
				sortable : true,
				formatter : function(value, row) {
					if (value == "0") {
						return "正常";
					} else {
						return "锁定";
					}
				}
			}, {
				field : 'id',
				title : '操作',
				formatter : function(value, row, index) {
					return $.BOOT.groupbtn(value, [ {
						cla : 'user_lock',
						text : "锁定解锁"
					}, {
						cla : 'user_delete',
						text : "删除账号"
					}, {
						cla : 'user_reset',
						text : "重置密码"
					}, {
						cla : 'user_edit',
						text : "编辑角色"
					} ]);
				}
			} ],
			paginationInfo : true
		});
		$('#filter-bar').bootstrapTableFilter({
			filters : [ {
				field : 'name',
				label : '姓名',
				type : 'search'
			}, {
				field : 'account',
				label : '账号',
				type : 'search'
			} ],
			connectTo : '#user_table'
		});
	});
	function rowStyle(row, index) {
		if (row.status == "1") {
			return {
				classes : 'danger'
			};
		}
		return {};
	}
	$.BOOT.click(".user_edit", function(c) {
		var id = $(c).attr("val");
		var href = $.webapp.root + '/admin/system/user/userrole_main_UI.do?id='
				+ id;
		$.BOOT.page("content_addroles", href, function() {
			$('#addrolesModal').modal('toggle');
		});
	});

	$.BOOT.click(".user_reset", function(c) {
		var href = $.webapp.root + '/admin/system/user/resetPwd.do';
		var id = $(c).attr("val");
		$.post(href, {
			id : id
		}, function(result) {
			$user_table.bootstrapTable('refresh');
			$.BOOT.toast1(result);
		}, 'json');
	});
	$.BOOT.click(".user_lock", function(c) {
		var id = $(c).attr("val");
		var href = $.webapp.root + '/admin/system/user/lockUser.do';
		$.post(href, {
			id : id
		}, function(result) {
			$user_table.bootstrapTable('refresh');
			$.BOOT.toast1(result);
		}, 'json');
	});
	$.BOOT.click(".user_delete", function(c) {
		var id = $(c).attr("val");
		var json = {
			title : "",
			text : "确定删除账号吗?",
			showCancelButton : true,
			type : 'warning',
			call : function() {
				var href = $.webapp.root + '/admin/system/user/delete.do';
				$.post(href, {
					ids : id
				}, function(result) {
					$user_table.bootstrapTable('refresh');
					$.BOOT.toast1(result);
				}, 'json');
			}
		};
		$.BOOT.alert(json, true);
	});
</script>