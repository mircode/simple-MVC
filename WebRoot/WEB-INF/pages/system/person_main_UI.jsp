<%@page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<jsp:include page="/template/modal.jsp"><jsp:param value="addusers" name="id" /><jsp:param value="编辑用户" name="title" /></jsp:include>
<section class="content">
	<div id="person-form-id"></div>
	<div id="filter-bar">
		<div class="btn-toolbar">
			<button type="button" class="btn btn-primary " id="person_add">添加</button>
		</div>
	</div>
	<table id="person_table" class="table-condensed table table-hover" data-row-style="rowStyle" data-side-pagination="server"></table>
</section>
<script type="text/javascript">
	var $person_table;
	$(function() {
		$person_table = $.BOOT.table("person_table", $.webapp.root
				+ "/admin/system/user/datagridperson.do", {
			columns : [ {
				field : 'name',
				title : '姓名',
			}, {
				field : 'sex',
				title : '性别',
			}, {
				field : 'email',
				title : '邮箱',
			}, {
				field : 'phone',
				title : '手机',
			}, {
				field : 'orgName',
				title : '部门',
			}, {
				field : 'positionName',
				title : '职位',
			}, {
				field : 'province',
				title : '省份'
			}, {
				field : 'city',
				title : '地市'
			}, {
				field : 'id',
				title : '操作',
				formatter : function(value, row, index) {
					return $.BOOT.groupbtn(value, [ {
						cla : 'person_edit',
						text : "编辑"
					}, {
						cla : 'person_delete',
						text : "删除"
					} ]);
				}
			} ],
			paginationInfo : true,
			showExport : true
		});
		$('#filter-bar').bootstrapTableFilter({
			filters : [ {
				field : 'name',
				label : '姓名',
				type : 'search'
			}, {
				field : 'phone',
				label : '手机',
				type : 'search'
			} ],
			connectTo : '#person_table'
		});
	});
	function rowStyle(row, index) {
		if (row.status == "1") {
			return {
				classes : 'danger'
			};
		}
		return {};
	}
	$.BOOT.click(".person_delete", function(c) {
		var id = $(c).attr("val");
		var json = {
			title : "",
			text : "确定删除用户吗?",
			showCancelButton : true,
			type : 'warning',
			call : function() {
				var href = $.webapp.root + '/admin/system/user/delete.do';
				$.post(href, {
					ids : id
				}, function(result) {
					$person_table.bootstrapTable('refresh');
					$.BOOT.toast1(result);
				}, 'json');
			}
		};
		$.BOOT.alert(json, true);
	});
	$.BOOT.click(".person_edit", function(c) {
		var id = $(c).attr("val");
		var href = $.webapp.root + '/admin/system/user/person_form_UI.do?id='
				+ id;
		$.BOOT.page("content_addusers", href, function() {
			$('#addusersModal').modal('toggle');
		});
	});
	$(document).on("click", "#person_add", function() {
		var href = $.webapp.root + '/admin/system/user/person_form_UI.do';
		$.BOOT.page("content_addusers", href, function() {
			$('#addusersModal').modal('toggle');
		});
	});
</script>