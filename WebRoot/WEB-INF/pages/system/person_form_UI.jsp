<%@page import="com.rosense.basic.util.LangUtils"%>
<%@page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<script>
	var form_url = $.webapp.root + "/admin/system/user/add.do";
	var parent_box, person_type;
	$(function() {
		//编辑，加载表单数据
		var orgtree = $.webapp.root + "/admin/system/org/tree.do";
		var ptree = $.webapp.root + "/admin/system/position/tree.do";
		var get = $.webapp.root + "/admin/system/user/get.do";
		$.BOOT.autoselect("orgId", orgtree, {
			title : "选择部门"
		});
		$.BOOT.autoselect("positionId", ptree, {
			title : "选择职位",
			callback : function() {
				if ($('input[name=id]').val().length > 0) {
					$.post(get, {
						id : $('input[name=id]').val()
					}, function(result) {
						form_url = $.webapp.root
								+ "/admin/system/user/update.do";
						$('#form_addusers').form('load', result);
					}, 'json');
				} else {
				}
			}
		});
	});
	$.BOOT.form("form_addusers", {
		name : {
			validators : {
				notEmpty : {
					message : '姓名不能为空'
				}
			}
		},
	}, function(params) {
		$.post(form_url, params, function(result) {
			$.BOOT.toast1(result);
			$person_table.bootstrapTable('refresh');
			modaladdusers();
		}, 'json');
	});
</script>
<div class="row">
	<input type="hidden" name="id" value="${id}">
	<div class="form-group col-md-6">
		<label for="account">账号<span style="color: gray;font-size: 12px;">密码默认123456</span></label> <input type="text" class="form-control" name="account" id="account" placeholder="输入账号">
	</div>
	<div class="form-group col-md-6">
		<label for="personName">姓名</label> <input type="text" class="form-control" name="name" id="personName" placeholder="输入姓名">
	</div>
	<div class="form-group col-md-6">
		<label for="email">邮箱</label> <input type="email" class="form-control" name="email" id="email" placeholder="输入邮箱">
	</div>
	<div class="form-group col-md-6">
		<label for="phone">手机</label> <input type="tel" class="form-control" name="phone" id="phone" placeholder="输入手机">
	</div>
	<div class="form-group col-md-6">
		<label for="phone">性别</label> <select class="form-control" id="sex" name="sex">
			<option value="男">男</option>
			<option value="女">女</option>
		</select>
	</div>
	<div class="form-group col-md-6">
		<label for="province">省份</label> <input type="text" class="form-control" name="province" id="province" placeholder="">
	</div>
	<div class="form-group col-md-6">
		<label for="city">地市</label> <input type="text" class="form-control" name="city" id="city" placeholder="">
	</div>
	<div class="form-group col-md-6">
		<label for="orgId">部门</label> <select class="form-control" id="orgId" name="orgId">
		</select>
	</div>
	<div class="form-group col-md-6">
		<label for="positionId">岗位</label> <select class="form-control" id="positionId" name="positionId">
		</select>
	</div>
</div>
