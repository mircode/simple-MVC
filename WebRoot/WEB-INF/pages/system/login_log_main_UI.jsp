<%@page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<script type="text/javascript">
	var $log_table;
	$(function() {
		$log_table = $.BOOT.table("log_table", $.webapp.root
				+ "/admin/system/log/datagridLL.do", {
			columns : [ {
				field : 'userName',
				title : '姓名'
			}, {
				field : 'browserType',
				title : '浏览器类型',
			}, {
				field : 'platformType',
				title : '平台'
			}, {
				field : 'ip',
				title : 'IP地址',
				width : 150,
			}, {
				field : 'created',
				title : '操作时间',
				width : 150,
				sortable : true
			} ],
			shows : false
		});
	});
</script>
<!-- Content Header (Page header) -->
<section class="content">
	<table id="log_table" class="table-condensed table table-hover" data-side-pagination="server"></table>
</section>