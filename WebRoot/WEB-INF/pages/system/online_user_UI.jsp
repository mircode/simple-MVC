<%@page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<script type="text/javascript">
	var $online_table;
	$(function() {
		$online_table = $.BOOT.table("online_table", $.webapp.root
				+ "/admin/system/online/datagrid.do", {
			columns : [ {
				field : 'account',
				title : '登陆账号',
			}, {
				field : 'name',
				title : '人员姓名',
			}, {
				field : 'orgName',
				title : '所属部门',
			}, {
				field : 'ip',
				title : 'IP地址',
			}, {
				field : 'loginTime',
				title : '登录时间',
			} ],
			shows : false,
		});
	});
</script>
<section class="content">
	<table id="online_table" class="table-condensed table table-hover" data-side-pagination="server"></table>
</section>