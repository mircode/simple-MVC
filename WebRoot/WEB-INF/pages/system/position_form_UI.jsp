<%@page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<script>
	var form_url = $.webapp.root + "/admin/system/position/add.do";
	$(function() {
		var tree = $.webapp.root + "/admin/system/position/tree.do";
		$.post(tree, {}, function(result) {
			$.BOOT.select("pid", result, "根目录");
			if ($('input[name=id]').val().length > 0) {
				var get = $.webapp.root + "/admin/system/position/get.do";
				$.post(get, {
					id : $('input[name=id]').val()
				}, function(result) {
					form_url = $.webapp.root + "/admin/system/position/update.do";
					$('#form_addposition').form('load', result);
				}, 'json');
			} else {
				$('#form_addposition').form('load', {
					pid : '${pid}'
				});
			}
		}, 'json');
	});
	$.BOOT.form("form_addposition", {
		name : {
			validators : {
				notEmpty : {
					message : '部门名称不能为空'
				}
			}
		}
	}, function(params) {
		$.post(form_url, params, function(result) {
			loadpositionTree();
			modaladdposition();
		}, 'json');
	});
</script>
<div>
	<input type="hidden" name="id" value="${id}">
	<div class="form-group">
		<label for="pid">父目录</label> <select class="form-control" id="pid" name="pid">
		</select>
	</div>
	<div class="form-group">
		<label for="positionName">名称</label> <input type="text" class="form-control" name="name" id="positionName" placeholder="输入部门名称">
	</div>
</div>
