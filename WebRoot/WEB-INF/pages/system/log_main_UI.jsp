<%@page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<script type="text/javascript">
	var $log_table;
	$(function() {
		$log_table = $.BOOT.table("log_table", $.webapp.root
				+ "/admin/system/log/datagrid.do", {
			columns : [ {
				field : 'userName',
				title : '用户名称',
			}, {
				field : 'title',
				title : '操作标题',
				sortable : true
			}, {
				field : 'detail',
				title : '详情',
				sortable : true
			}, {
				field : 'ip',
				title : 'IP地址',
				width : 150,
			}, {
				field : 'created',
				title : '操作时间',
				width : 150,
				sortable : true
			} ],
			shows : false
		});
	});
</script>
<section class="content">
	<table id="log_table" class="table-condensed table table-hover" data-side-pagination="server"></table>
</section>