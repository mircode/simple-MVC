<%@page import="com.rosense.basic.util.cons.Const"%>
<%@page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<style>
<!--
.notifications-menu li {
	white-space: nowrap;
	text-overflow: ellipsis;
	-o-text-overflow: ellipsis;
	overflow: hidden;
}
-->
</style>
<div class="wrapper">
	<header class="main-header" id="main-header">
		<!-- Logo -->
		<a href="javascript:void;" class="logo"> <span class="logo-mini"><b>MVC</b></span> <span class="logo-lg"><b>Simple MVC</b></span>
		</a>
		<!-- Header Navbar: style can be found in header.less -->
		<nav class="navbar navbar-static-top" role="navigation">
			<!-- Sidebar toggle button-->
			<a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button"> </a> <input type="hidden" id="index-p-id" value="${p}">
			<div class="navbar-custom-menu">
				<ul class="nav navbar-nav">
					<!-- Notifications: style can be found in dropdown.less -->
					<jsp:include page="../../template/skin.jsp"></jsp:include>
					<li class="dropdown notifications-menu"><a href="#" class="dropdown-toggle" data-toggle="dropdown"> <i id="shake-bell-id" class="fa fa-bell-o shake "></i> <span>&nbsp;</span><span class="label label-warning " style="right: 10px;" id="notice-id-count">0</span>
					</a>
						<ul class="dropdown-menu" id="notice-id">
						</ul></li>
					<!-- User Account: style can be found in dropdown.less -->
					<li class="dropdown user user-menu"><a href="#" class="dropdown-toggle" data-toggle="dropdown"> <img src="/common/photo.do?id=${userId}" class="user-image" style="border: 2px solid white;" alt="User Image"> <span class="hidden-xs">${userName}</span> <span>&nbsp;</span>
					</a>
						<ul class="dropdown-menu">
							<!-- User image -->
							<li class="user-header"><img style="cursor: pointer;" src="/common/photo.do?id=${userId}" class="img-circle" alt=""> <%-- <p>${score}积分</p> --%>
								<p>${userName}</p></li>
							<!-- Menu Body -->
							<!-- <li class="user-body">
								<div class="col-xs-12 text-center">
								</div>
							</li> -->
							<!-- Menu Footer-->
							<li class="user-footer">
								<div class="pull-left">
									<a href="#" class="btn btn-default btn-flat" onclick="$('#menu-gerenziliao').click();">个人资料</a>
								</div>
								<div class="pull-right">
									<a href="#" class="btn btn-default btn-flat" id="logout_btn_id" onclick="$.BOOT.logout('login.jsp');">注销</a>
								</div>
							</li>
						</ul></li>
					<!-- Control Sidebar Toggle Button -->
					<!-- <li><a href="#" data-toggle="control-sidebar"><i
							class="fa fa-gears"></i></a></li> -->
				</ul>
			</div>
		</nav>
	</header>
	<!-- Left side column. contains the logo and sidebar -->
	<aside class="main-sidebar">
		<!-- sidebar: style can be found in sidebar.less -->
		<section class="sidebar" id="sidebar-menu" style="overflow-y:auto; "></section>
		<!-- /.sidebar -->
	</aside>
	<!-- Content Wrapper. Contains page content -->
	<div class="content-wrapper">
		<ul id="simple-tabs" class="nav nav-tabs">
		</ul>
		<div id="simle-tab_content" class="tab-content"></div>
	</div>
	<div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->
<script type="text/javascript">
	var cache_json = "${cache}".replace(/=/g, ":");
	cache_json = eval("(" + cache_json + ")");
	$(function() {
		$.BOOT.notice();
		$.BOOT.navmenu("sidebar-menu", function() {
			$.BOOT.posmenu();
		});
		var noticeCount = $("#notice-id-count").val() + 0;
		if (noticeCount > 0) {
			$("#shake-bell-id").addClass("shake-constant");
		}
		$('body').oneTime('3s', 'S', function() {
			$("#shake-bell-id").removeClass("shake-constant");
		});
		$("#sidebar-menu").css("height", $.webapp.getInner().height - 50);
	});
</script>