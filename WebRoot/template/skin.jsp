<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<style>
<!--
.skin-menu .dropdown-menu>li>a {
	padding: 0px;
}
-->
</style>
<li class="dropdown notifications-menu skin-menu"><a href="#" class="dropdown-toggle" data-toggle="dropdown">样式 </a>
	<ul class="dropdown-menu list-unstyled clearfix">
		<li style="float:left; width: 50%; padding: 5px;"><a href="javascript:void(0);" data-skin="skin-green-light" style="display: block; box-shadow: 0 0 3px rgba(0,0,0,0.4)" class="clearfix full-opacity-hover"><div>
					<span style="display:block; width: 20%; float: left; height: 7px;" class="bg-green-active"></span><span class="bg-green" style="display:block; width: 80%; float: left; height: 7px;"></span>
				</div>
				<div>
					<span style="display:block; width: 20%; float: left; height: 20px; background: #f9fafc;"></span><span style="display:block; width: 80%; float: left; height: 20px; background: #f4f5f7;"></span>
				</div></a>
			<p class="text-center no-margin" style="font-size: 12px">绿色 浅</p></li>
		<li style="float:left; width: 50%; padding: 5px;"><a href="javascript:void(0);" data-skin="skin-green" style="display: block; box-shadow: 0 0 3px rgba(0,0,0,0.4)" class="clearfix full-opacity-hover"><div>
					<span style="display:block; width: 20%; float: left; height: 7px;" class="bg-green-active"></span><span class="bg-green" style="display:block; width: 80%; float: left; height: 7px;"></span>
				</div>
				<div>
					<span style="display:block; width: 20%; float: left; height: 20px; background: #222d32;"></span><span style="display:block; width: 80%; float: left; height: 20px; background: #f4f5f7;"></span>
				</div></a>
			<p class="text-center no-margin">绿色</p></li>
		<li style="float:left; width: 50%; padding: 5px;"><a href="javascript:void(0);" data-skin="skin-blue-light" style="display: block; box-shadow: 0 0 3px rgba(0,0,0,0.4)" class="clearfix full-opacity-hover"><div>
					<span style="display:block; width: 20%; float: left; height: 7px; background: #367fa9;"></span><span class="bg-light-blue" style="display:block; width: 80%; float: left; height: 7px;"></span>
				</div>
				<div>
					<span style="display:block; width: 20%; float: left; height: 20px; background: #f9fafc;"></span><span style="display:block; width: 80%; float: left; height: 20px; background: #f4f5f7;"></span>
				</div></a>
			<p class="text-center no-margin" style="font-size: 12px">蓝色 浅</p></li>
		<li style="float:left; width: 50%; padding: 5px;"><a href="javascript:void(0);" data-skin="skin-blue" style="display: block; box-shadow: 0 0 3px rgba(0,0,0,0.4)" class="clearfix full-opacity-hover"><div>
					<span style="display:block; width: 20%; float: left; height: 7px; background: #367fa9;"></span><span class="bg-light-blue" style="display:block; width: 80%; float: left; height: 7px;"></span>
				</div>
				<div>
					<span style="display:block; width: 20%; float: left; height: 20px; background: #222d32;"></span><span style="display:block; width: 80%; float: left; height: 20px; background: #f4f5f7;"></span>
				</div></a>
			<p class="text-center no-margin">蓝色</p></li>
		<li style="float:left; width: 50%; padding: 5px;"><a href="javascript:void(0);" data-skin="skin-purple-light" style="display: block; box-shadow: 0 0 3px rgba(0,0,0,0.4)" class="clearfix full-opacity-hover"><div>
					<span style="display:block; width: 20%; float: left; height: 7px;" class="bg-purple-active"></span><span class="bg-purple" style="display:block; width: 80%; float: left; height: 7px;"></span>
				</div>
				<div>
					<span style="display:block; width: 20%; float: left; height: 20px; background: #f9fafc;"></span><span style="display:block; width: 80%; float: left; height: 20px; background: #f4f5f7;"></span>
				</div></a>
			<p class="text-center no-margin" style="font-size: 12px">紫色 浅</p></li>
		<li style="float:left; width: 50%; padding: 5px;"><a href="javascript:void(0);" data-skin="skin-purple" style="display: block; box-shadow: 0 0 3px rgba(0,0,0,0.4)" class="clearfix full-opacity-hover"><div>
					<span style="display:block; width: 20%; float: left; height: 7px;" class="bg-purple-active"></span><span class="bg-purple" style="display:block; width: 80%; float: left; height: 7px;"></span>
				</div>
				<div>
					<span style="display:block; width: 20%; float: left; height: 20px; background: #222d32;"></span><span style="display:block; width: 80%; float: left; height: 20px; background: #f4f5f7;"></span>
				</div></a>
			<p class="text-center no-margin">紫色</p></li>
		<li style="float:left; width: 50%; padding: 5px;"><a href="javascript:void(0);" data-skin="skin-red-light" style="display: block; box-shadow: 0 0 3px rgba(0,0,0,0.4)" class="clearfix full-opacity-hover"><div>
					<span style="display:block; width: 20%; float: left; height: 7px;" class="bg-red-active"></span><span class="bg-red" style="display:block; width: 80%; float: left; height: 7px;"></span>
				</div>
				<div>
					<span style="display:block; width: 20%; float: left; height: 20px; background: #f9fafc;"></span><span style="display:block; width: 80%; float: left; height: 20px; background: #f4f5f7;"></span>
				</div></a>
			<p class="text-center no-margin" style="font-size: 12px">红色 浅</p></li>
		<li style="float:left; width: 50%; padding: 5px;"><a href="javascript:void(0);" data-skin="skin-red" style="display: block; box-shadow: 0 0 3px rgba(0,0,0,0.4)" class="clearfix full-opacity-hover"><div>
					<span style="display:block; width: 20%; float: left; height: 7px;" class="bg-red-active"></span><span class="bg-red" style="display:block; width: 80%; float: left; height: 7px;"></span>
				</div>
				<div>
					<span style="display:block; width: 20%; float: left; height: 20px; background: #222d32;"></span><span style="display:block; width: 80%; float: left; height: 20px; background: #f4f5f7;"></span>
				</div></a>
			<p class="text-center no-margin">红色</p></li>
		<li style="float:left; width: 50%; padding: 5px;"><a href="javascript:void(0);" data-skin="skin-yellow-light" style="display: block; box-shadow: 0 0 3px rgba(0,0,0,0.4)" class="clearfix full-opacity-hover"><div>
					<span style="display:block; width: 20%; float: left; height: 7px;" class="bg-yellow-active"></span><span class="bg-yellow" style="display:block; width: 80%; float: left; height: 7px;"></span>
				</div>
				<div>
					<span style="display:block; width: 20%; float: left; height: 20px; background: #f9fafc;"></span><span style="display:block; width: 80%; float: left; height: 20px; background: #f4f5f7;"></span>
				</div></a>
			<p class="text-center no-margin" style="font-size: 12px;">黄色 浅</p></li>
		<li style="float:left; width: 50%; padding: 5px;"><a href="javascript:void(0);" data-skin="skin-yellow" style="display: block; box-shadow: 0 0 3px rgba(0,0,0,0.4)" class="clearfix full-opacity-hover"><div>
					<span style="display:block; width: 20%; float: left; height: 7px;" class="bg-yellow-active"></span><span class="bg-yellow" style="display:block; width: 80%; float: left; height: 7px;"></span>
				</div>
				<div>
					<span style="display:block; width: 20%; float: left; height: 20px; background: #222d32;"></span><span style="display:block; width: 80%; float: left; height: 20px; background: #f4f5f7;"></span>
				</div></a>
			<p class="text-center no-margin">黄色</p></li>
	</ul></li>
<script type="text/javascript">
	$(function() {
		var cla = $.BOOT.getCookie("sidebar");
		if (cla != "") {
			var skinbar = $(".sidebar-mini");
			skinbar.attr("class", "");
			skinbar.addClass("sidebar-mini fixed " + cla);
		}
	});
	$(document).on("click", ".list-unstyled li a", function() {
		var v = $(this).attr("data-skin");
		var skinbar = $(".sidebar-mini");
		skinbar.attr("class", "");
		skinbar.addClass("sidebar-mini fixed " + v);
		$.BOOT.setCookie("sidebar", v);
	});
</script>